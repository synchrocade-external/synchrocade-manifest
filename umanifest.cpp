/**
 *  This work is distributed under the General Public License,
 *	see LICENSE for details
 *
 *  @author Gwenna�l ARBONA
 **/

#include "umanifest.h"
#include "ui_umanifest.h"

#include <QDebug>
#include <QDirIterator>


/*----------------------------------------------
          Constructor & destructor
----------------------------------------------*/

UManifest::UManifest(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::UManifest)
{
    ui->setupUi(this);

    ui->releaseNotes->append("Searching for files...");

    QDomDocument* dom = new QDomDocument("files");
    QDomElement root = dom->createElement("FolderProperties");
    root.setAttribute("FolderName", ".");
    dom->appendChild(root);

    this->GenerateManifest(dom, root, QDir::currentPath());

    ui->releaseNotes->append("Writing manifest to XML...");

    QFile* file = new QFile("GameManifest.xml.compressed");
    if (file->open(QIODevice::WriteOnly | QIODevice::Text ))
    {
        QByteArray manifest = dom->toByteArray();
        int compress_level = 9; // compression level
        file->write(qCompress(manifest, compress_level)); // read input, compress and write to output is a single line of code
        file->close();
    }

    ui->releaseNotes->append("Done !");

    delete dom;
    delete file;

    exit(0);
}

UManifest::~UManifest()
{
    delete ui;
}


/*----------------------------------------------
          Private methods
----------------------------------------------*/

void UManifest::GenerateManifest(QDomDocument* dom, QDomElement root, QString dir)
{
    QDomElement folders = dom->createElement("Folders");
    root.appendChild(folders);

    QDomElement files = dom->createElement("Files");
    root.appendChild(files);

    QDirIterator it(dir);
    while (it.hasNext())
    {
        QString filePath = it.next();
        QFileInfo fi(filePath);

        //compress the release notes, but don't delete the raw xml to make it easier
        //and don't add to the manifest
        if(fi.fileName() == "ReleaseNotes.xml")
        {
            QFile* file = new QFile(filePath);
            QFile* compressed = new QFile(filePath+".compressed");
            if(file->open(QIODevice::ReadOnly)&& compressed->open(QFile::WriteOnly))
            {
                int compress_level = 9; // compression level
                compressed->write(qCompress(file->readAll(), compress_level)); // read input, compress and write to output is a single line of code
                file->close();
                compressed->close();
            }
            continue;
        }
        if (fi.fileName().startsWith("UnrealManifest")
         || fi.fileName() == MANIFEST_NAME
         ||(fi.fileName().startsWith("Qt5") && fi.fileName().endsWith(".dll")))
        {
            continue;
        }

        if (QDir(filePath).exists())
        {
            if (!filePath.endsWith("."))
            {
                QDomElement folder = dom->createElement("FolderProperties");

                folder.setAttribute("FolderName", fi.fileName());
                root.appendChild(folder);

                GenerateManifest(dom, folder, filePath);
            }
        }
        else
        {
            QDomElement e = dom->createElement("FileProperties");
            QString fileHash = HashFile(filePath);

            if (fileHash.length() > 0)
            {
                e.setAttribute("FileName", fi.fileName());
                e.setAttribute("Size", fi.size());
                e.setAttribute("md5", fileHash);
                ui->releaseNotes->append(filePath  + " : " + fileHash);
            }
            else
            {
                e.setTagName("EmptyFile");
            }

            files.appendChild(e);
        }
    }
}

QString UManifest::HashFile(QString filePath)
{
    QFile* file = new QFile(filePath);
    QFile* compressed = new QFile(filePath+".compressed");
    QByteArray hashed;

    if (file->open(QIODevice::ReadOnly)&& compressed->open(QFile::WriteOnly))
    {
        QByteArray content = file->readAll();
        hashed = QCryptographicHash::hash(content, QCryptographicHash::Md5);
        file->seek(0);
        int compress_level = 9; // compression level
        compressed->write(qCompress(file->readAll(), compress_level)); // read input, compress and write to output is a single line of code
        file->remove();
        compressed->close();
    }
    delete file;
    delete compressed;
    return hashed.toHex().data();
}
